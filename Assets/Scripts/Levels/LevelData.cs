﻿public class LevelData
{
    // Time from alpha
    public float Time { get; set; }
    // Projectiles to spawn at this time
    public Projectile[] Projectiles { get; set; }
    // Word projectiles to spawn at this time
    public WordProjectile[] WordProjectiles { get; set; }
    // Events to occur at this time
    public Event[] Events { get; set; }

    public LevelData(float time, Projectile[] projectiles, WordProjectile[] wordProjectiles, Event[] events)
    {
        Time = time;
        Projectiles = projectiles;
        WordProjectiles = wordProjectiles;
        Events = events;
    }
}
