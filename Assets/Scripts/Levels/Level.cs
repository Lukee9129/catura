﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Assets.Scripts.Levels
{
    public enum Difficulty
    {
        Easy, Medium, Hard, Impossible
    }
    public class Level
    {
        public string SongTitle { get; }
        public string ArtistName { get; }
        public Difficulty LevelDifficulty { get; }
        public AudioClip Song { get; }
        public LevelData[] Data { get; }

        public Level(string songTitle, string artistName, Difficulty levelDifficulty, AudioClip song, LevelData[] data)
        {
            SongTitle = songTitle;
            ArtistName = artistName;
            LevelDifficulty = levelDifficulty;
            Song = song;
            Data = data;
        }
    }
}