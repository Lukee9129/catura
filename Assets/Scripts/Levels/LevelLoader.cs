﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LevelLoader : MonoBehaviour
{
    public static int LoadedLevel = 0;
    void Start()
    {
        if (!SceneController.IsPaused)
            MenuMusicController.MenuMusic.go.SetActive(true); // Ensure menu music is enabled when on this page
    }
    public void LoadLevel(int levelNumber)
    {
        SceneController.IsPaused = false;
        MenuMusicController.MenuMusic.go.SetActive(false); // Disable menu music when going into the game
        DebugModule.ClearActiveProjectiles(); // If we're still tracking projectiles from a previous playthrough, clear them.
        LoadedLevel = levelNumber;
        SceneManager.LoadScene("Main");
    }

    public void RetryLevel()
    {
        LoadLevel(LoadedLevel);
    }
}
