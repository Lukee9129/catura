﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using Assets.Scripts.Levels;
using UnityEngine;
using UnityEngine.UI;

public class LevelController : MonoBehaviour
{
    private static readonly Dictionary<int, Level> levels = new Dictionary<int, Level>(); //<Level Number, Level Data>
    private readonly Dictionary<int, string> fileNames = new Dictionary<int, string>(); //<Level number, file name>
    private readonly Dictionary<string, Font> fontCache = new Dictionary<string, Font>();
    private Player player;
    private MusicController musicController;
    private ProjectileSpawner projectileSpawner;

    // Splash screen text
    private CanvasGroup splashCanvas;
    private Text artistName;
    private Text songTitle;
    private Text difficulty;

    // Variables used to keep track of the current level
    private bool isPlaying = false;
    private Level currentLevel;
    private float currentTime;

    // Start is called before the first frame update
    void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player").GetComponent<Player>();
        musicController = GameObject.Find("Music Controller").GetComponent<MusicController>();
        projectileSpawner = GameObject.Find("Projectile Spawner").GetComponent<ProjectileSpawner>();
        splashCanvas = GameObject.Find("Splash Canvas").GetComponent<CanvasGroup>();
        artistName = GameObject.Find("Artist Name").GetComponent<Text>();
        songTitle = GameObject.Find("Song Title").GetComponent<Text>();
        difficulty = GameObject.Find("Difficulty").GetComponent<Text>();
        splashCanvas.alpha = 0; // Initially hide the splash

        TryStartLevel(LevelLoader.LoadedLevel);
    }

    void Awake()
    {
        // Add all our file names here
        fileNames.Add(1, "It's Hard to Say Goodbye");
        fileNames.Add(2, "Down");
    }

    public void TryStartLevel(int levelNumber)
    {
        if (!fileNames.ContainsKey(levelNumber))
        {
            Debug.Log("Tried to load level that doesn't exist!");
            return;
        }
        bool result = LoadLevel(levelNumber);
        if (result)
        {
            StartLevel(levelNumber);
        }
        else
        {
            Debug.Log("Failed to load level!");
        }
    }

    /* LEVEL DATA FILE FORMAT:
     * [meta]
     * Song Title
     * Artist Name
     * Difficulty
     * [/meta]
     * [proj]
     * projectile data / word projectile data / event data (see ConstructProjectile, ConstructWordProjectile or ConstructEvent method)
     * [/proj]
     */
    private bool LoadLevel(int levelNumber, bool forceReload = false)
    {
        // If already loaded just return true
        if (!forceReload && levels.ContainsKey(levelNumber)) return true;

        if (!fileNames.TryGetValue(levelNumber, out string fileName))
        {
            Debug.Log($"No file name has been stored for level number: {levelNumber}");
        }

        // Load music file
        AudioClip musicFile = Resources.Load<AudioClip>($"Music/{fileName}");
        if (musicFile == null)
        {
            Debug.Log("Audio file missing!");
            return false;
        }
        
        // Load level data file
        TextAsset levelDataFile = Resources.Load<TextAsset>($"Levels/{fileName}");
        if (levelDataFile == null)
        {
            Debug.Log("Level data file missing!");
            return false;
        }

        Match metaMatch = Regex.Match(levelDataFile.text, "(?<=\\[meta\\]\r\n).+(?=\r\n\\[\\/meta\\])", RegexOptions.Singleline);
        if (!metaMatch.Success)
        {
            Debug.Log("Metadata missing from level data file!");
            return false;
        }
        string[] metaLines = metaMatch.Value.Split(new[] {"\r\n"}, StringSplitOptions.None);
        if (metaLines.Length != 3)
        {
            Debug.Log("Metadata incomplete in level data file!");
            return false;
        }
        Match projMatch = Regex.Match(levelDataFile.text, "(?<=\\[proj\\]\r\n).+(?=\r\n\\[\\/proj\\])", RegexOptions.Singleline);
        if (!projMatch.Success)
        {
            Debug.Log("Projectiles missing from level data file!");
            return false;
        }
        string[] projLines = projMatch.Value.Split(new[] { "\r\n" }, StringSplitOptions.None);

        float lastTime = -1;
        List<Projectile> projectiles = new List<Projectile>();
        List<WordProjectile> wordProjectiles = new List<WordProjectile>();
        List<Event> events = new List<Event>();
        List<LevelData> data = new List<LevelData>();
        foreach (string line in projLines)
        {
            if (line.StartsWith("#")) continue; //Comment line
            string[] values = line.Split(',');

            float time;
            if (values.Length == 10) // normal projectile
            {
                // Send line for debug purposes, send the last projectile time to ensure everything is in chronological order
                bool success = ConstructProjectile(values, line, lastTime, out time, out bool shouldAbort, out Projectile nextProjectile);
                if (shouldAbort) return false; // Fatal error, abort the entire thing
                if (!success) continue;

                if (lastTime >= 0 && !Mathf.Approximately(time, lastTime))
                {
                    // Add all the projectile data that has occurred at this time, we're now moving on to the next tick
                    LevelData levelData = new LevelData(lastTime, projectiles.ToArray(), wordProjectiles.ToArray(), events.ToArray());
                    data.Add(levelData);
                    projectiles.Clear();
                    wordProjectiles.Clear();
                    events.Clear();
                }

                projectiles.Add(nextProjectile);
            }
            else if (values.Length == 12) // word projectile
            {
                bool success = ConstructWordProjectile(values, line, lastTime, out time, out bool shouldAbort, out WordProjectile nextWordProjectile);
                if (shouldAbort) return false;
                if (!success) continue;

                if (lastTime >= 0 && !Mathf.Approximately(time, lastTime))
                {
                    LevelData levelData = new LevelData(lastTime, projectiles.ToArray(), wordProjectiles.ToArray(), events.ToArray());
                    data.Add(levelData);
                    projectiles.Clear();
                    wordProjectiles.Clear();
                    events.Clear();
                }

                wordProjectiles.Add(nextWordProjectile);
            }
            else if (values.Length == 2) // event
            {
                bool success = ConstructEvent(values, line, lastTime, out time, out bool shouldAbort, out Event nextEvent);
                if (shouldAbort) return false;
                if (!success) continue;

                if (lastTime >= 0 && !Mathf.Approximately(time, lastTime))
                {
                    LevelData levelData = new LevelData(lastTime, projectiles.ToArray(), wordProjectiles.ToArray(), events.ToArray());
                    data.Add(levelData);
                    projectiles.Clear();
                    wordProjectiles.Clear();
                    events.Clear();
                }

                events.Add(nextEvent);
            }
            else
            {
                Debug.Log($"Line: {line}\nDoes not contain the required amount of values (10 for projectile or 12 for word projectile)");
                continue;
            }

            lastTime = time;
        }

        data.Add(new LevelData(lastTime, projectiles.ToArray(), wordProjectiles.ToArray(), events.ToArray())); // Add last set of projectiles

        Difficulty difficulty;
        switch (metaLines[2])
        {
            default:
            case "Easy":
                difficulty = Difficulty.Easy;
                break;
            case "Medium":
                difficulty = Difficulty.Medium;
                break;
            case "Hard":
                difficulty = Difficulty.Hard;
                break;
            case "Impossible":
                difficulty = Difficulty.Impossible;
                break;

        }
        Level level = new Level(metaLines[0], metaLines[1], difficulty, musicFile, data.ToArray());
        levels.Add(levelNumber, level);
        return true;
    }

    // format:
    // time since alpha, projectile type, start viewport x, start viewport y, end viewport x, end viewport y, linear move speed, initial rotation, rotational move speed, order
    private bool ConstructProjectile(string[] values, string line, float lastProjectileTime, out float time, out bool shouldAbort, out Projectile projectile)
    {
        projectile = null;
        shouldAbort = false;
        
        if (!float.TryParse(values[0], out time))
        {
            Debug.Log($"Failed to parse time for line: {line}");
            return false;
        }

        if (time < lastProjectileTime)
        {
            Debug.Log("Projectile data is not in chronological order! Aborting!");
            shouldAbort = true;
            return false;
        }

        if (!ProjectileLoader.ProjectileTypes.ContainsKey(values[1]))
        {
            Debug.Log($"Projectile {values[1]} unrecognised. Has it been loaded?");
            return false;
        }
        ProjectileType projectileType = ProjectileLoader.ProjectileTypes[values[1]];

        if (!float.TryParse(values[2], out float startX))
        {
            Debug.Log($"Failed to parse startX for line: {line}");
            return false;
        }
        if (!float.TryParse(values[3], out float startY))
        {
            Debug.Log($"Failed to parse startY for line: {line}");
            return false;
        }
        if (!float.TryParse(values[4], out float endX))
        {
            Debug.Log($"Failed to parse endX for line: {line}");
            return false;
        }
        if (!float.TryParse(values[5], out float endY))
        {
            Debug.Log($"Failed to parse endY for line: {line}");
            return false;
        }

        if (!float.TryParse(values[6], out float moveSpeed))
        {
            Debug.Log($"Failed to parse moveSpeed for line: {line}");
            return false;
        }
        if (!float.TryParse(values[7], out float initialRotation))
        {
            Debug.Log($"Failed to parse initialRotation for line: {line}");
            return false;
        }
        if (!float.TryParse(values[8], out float rotationSpeed))
        {
            Debug.Log($"Failed to parse rotationSpeed for line: {line}");
            return false;
        }

        if (!int.TryParse(values[9], out int order))
        {
            Debug.Log($"Failed to parse order for line: {line}");
            return false;
        }

        Vector2 start = Camera.main.ViewportToWorldPoint(new Vector3(startX, startY, Camera.main.nearClipPlane));
        Vector2 end = Camera.main.ViewportToWorldPoint(new Vector3(endX, endY, Camera.main.nearClipPlane));

        if (projectileType.OffsetAnchor)
        {
            // These projectiles are spawned on the edge of the screen. Take their width and height into consideration (effectively moving their anchor).
            OffsetProjectileLocation(ref start, startX, startY, initialRotation, projectileType);
            OffsetProjectileLocation(ref end, endX, endY, initialRotation, projectileType);
        }

        projectile = new Projectile(projectileType, start, end, moveSpeed, initialRotation, rotationSpeed, order);
        return true;
    }

    // format:
    // time since alpha, word_<word>, font size (pt), font name, font color r, font color g, font color b, location x, location y, is from top (0 or 1), duration, speed to reach destination
    private bool ConstructWordProjectile(string[] values, string line, float lastProjectileTime, out float time, out bool shouldAbort, out WordProjectile wordProjectile)
    {
        wordProjectile = null;
        shouldAbort = false;

        if (!float.TryParse(values[0], out time))
        {
            Debug.Log($"Failed to parse time for line: {line}");
            return false;
        }

        if (time < lastProjectileTime)
        {
            Debug.Log("Projectile data is not in chronological order! Aborting!");
            shouldAbort = true;
            return false;
        }

        if (!values[1].StartsWith("word_"))
        {
            Debug.Log($"Word projectile does not have word string in the correct format for line: {line}");
            return false;
        }

        string word = values[1].Substring(values[1].IndexOf('_') + 1); // Get substring of word from first underscore (allows underscore in the word)

        if (!int.TryParse(values[2], out int fontSize))
        {
            Debug.Log($"Failed to parse fontSize for line: {line}");
            return false;
        }

        string fontName = values[3];
        Font font;
        if (fontCache.ContainsKey(fontName))
        {
            font = fontCache[fontName];
        }
        else
        {
            font = Resources.Load<Font>($"Fonts/{fontName}");
            if (font == null) font = Resources.GetBuiltinResource<Font>("Arial.ttf"); // Doesn't exist, use default font instead
            fontCache[fontName] = font;
        }

        if (!float.TryParse(values[4], out float r))
        {
            Debug.Log($"Failed to parse font color r for line: {line}");
            return false;
        }
        if (!float.TryParse(values[5], out float g))
        {
            Debug.Log($"Failed to parse font color g for line: {line}");
            return false;
        }
        if (!float.TryParse(values[6], out float b))
        {
            Debug.Log($"Failed to parse font color b for line: {line}");
            return false;
        }

        Color fontColor = new Color(r, g, b);

        if (!float.TryParse(values[7], out float x))
        {
            Debug.Log($"Failed to parse location x for line: {line}");
            return false;
        }
        if (!float.TryParse(values[8], out float y))
        {
            Debug.Log($"Failed to parse location y for line: {line}");
            return false;
        }

        Vector2 location = new Vector2(x, y);

        bool isTopDown = values[9] == "1";

        if (!float.TryParse(values[10], out float duration))
        {
            Debug.Log($"Failed to parse duration for line: {line}");
            return false;
        }

        if (!float.TryParse(values[11], out float speed))
        {
            Debug.Log($"Failed to parse speed for line: {line}");
            return false;
        }

        wordProjectile = new WordProjectile(word, fontSize, font, fontColor, location, isTopDown, duration, speed);
        return true;
    }

    // format:
    // time since alpha, event_<event name>
    private bool ConstructEvent(string[] values, string line, float lastProjectileTime, out float time, out bool shouldAbort, out Event requestedEvent)
    {
        requestedEvent = null;
        shouldAbort = false;

        if (!float.TryParse(values[0], out time))
        {
            Debug.Log($"Failed to parse time for line: {line}");
            return false;
        }

        if (time < lastProjectileTime)
        {
            Debug.Log("Projectile data is not in chronological order! Aborting!");
            shouldAbort = true;
            return false;
        }

        if (!values[1].StartsWith("event_"))
        {
            Debug.Log($"Event does not have event name in the correct format for line: {line}");
            return false;
        }

        string name = values[1].Substring(values[1].IndexOf('_') + 1); // Get substring of event from first underscore

        if (!EventLoader.Events.ContainsKey(name))
        {
            Debug.Log($"Event {name} unrecognised! Has it been loaded?");
            return false;
        }


        requestedEvent = EventLoader.Events[name];
        return true;
    }

    private void OffsetProjectileLocation(ref Vector2 location, float x, float y, float initialRotation, ProjectileType projectileType)
    {
        // Calculate offset taking rotation of the shape into account
        float heightOffset;
        float widthOffset;
        if (Mathf.Approximately(initialRotation, 0))
        {
            // If no initial rotation, skip some cycles on the ALU for performance
            heightOffset = projectileType.Height / 2;
            widthOffset = projectileType.Width / 2;
        }
        else
        {
            float radRotation = (Mathf.PI / 180) * initialRotation;
            float sinTheta = Mathf.Abs(Mathf.Sin(radRotation));
            float cosTheta = Mathf.Abs(Mathf.Cos(radRotation));
            heightOffset = (sinTheta * projectileType.Width + cosTheta * projectileType.Height) / 2;
            widthOffset = (sinTheta * projectileType.Height + cosTheta * projectileType.Width) / 2;
        }

        if (Mathf.Approximately(x, 0))
            location.x -= widthOffset;
        else if (Mathf.Approximately(x, 1))
            location.x += widthOffset;

        if (Mathf.Approximately(y, 0))
            location.y -= heightOffset;
        else if (Mathf.Approximately(y, 1))
            location.y += heightOffset;
    }
    private void StartLevel(int levelNumber)
    {
        if (levels.ContainsKey(levelNumber))
        {
            currentLevel = levels[levelNumber];
            musicController.LoadSong(currentLevel.Song);
            float volume = PlayerPrefs.GetFloat("Volume", 1);
            musicController.SetVolume(volume); // Set volume based on player settings
            songTitle.text = currentLevel.SongTitle;
            artistName.text = currentLevel.ArtistName;
            difficulty.text = currentLevel.LevelDifficulty.ToString();
            StartCoroutine(DisplaySplashAndStart());
        }
        else
        {
            Debug.Log("Tried to start level before loading it.");
        }
    }

    private IEnumerator DisplaySplashAndStart() // Title card
    {
        player.isLocked = true; // Lock player movement
        SceneController.IsPaused = true; // Disable pausing
        // Fade text in
        for (float f = 0; f <= 1; f += 0.01f)
        {
            splashCanvas.alpha = Mathf.Lerp(0, 1, f);
            yield return new WaitForSeconds(0.01f);
        }
        yield return new WaitForSeconds(2); // Wait
        // Fade text out
        for (float f = 1; f >= 0; f -= 0.01f)
        {
            splashCanvas.alpha = Mathf.Lerp(0, 1, f);
            yield return new WaitForSeconds(0.01f);
        }

        player.isLocked = false; // Unlock player movement
        SceneController.IsPaused = false; // Allow pausing
        isPlaying = true;
        currentTime = 0;
        musicController.Play();
        StartCoroutine(SpawnProjectiles());
    }

    private IEnumerator SpawnProjectiles()
    {
        int nextIndex = 0;
        int count = currentLevel.Data.Length;
        do
        {
            if (nextIndex < count) // If there are more projectiles to spawn
            {
                LevelData nextLevelData = currentLevel.Data[nextIndex++]; // Get the next one
                if (nextLevelData.Time <= currentTime) // If it should have been spawned by now...
                {
                    projectileSpawner.SpawnProjectiles(nextLevelData.Projectiles, nextLevelData.Time); // ...spawn it
                    projectileSpawner.SpawnWords(nextLevelData.WordProjectiles, nextLevelData.Time);
                    projectileSpawner.RunEvents(nextLevelData.Events, nextLevelData.Time); // Run events
                }
                else
                {
                    nextIndex--; // Otherwise, let the next iteration handle it
                    float timeUntilNextProjectile = (nextLevelData.Time - currentTime) / 1000;
                    yield return new WaitForSeconds(timeUntilNextProjectile); // Yield until next projectile is to be spawned
                }
            }
            else
            {
                isPlaying = false; // All projectiles spawned
            }
        } while (isPlaying);
    }

    void Update()
    {
        if (isPlaying)
            currentTime += Time.deltaTime * 1000; // If playing keep track of time since alpha so we know when to spawn projectiles
    }
}
