﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneController : MonoBehaviour
{
    public static bool IsPaused = false;
    private static MusicController musicController;
    public void LoadScene(int index)
    {
        IsPaused = false;
        SceneManager.LoadScene(index);
    }

    public void ResumeGame()
    {
        GetMusicController().TogglePaused();
        Time.timeScale = 1;
        IsPaused = false;
        SceneManager.UnloadSceneAsync(6);
    }

    public static void PauseGame() // No called directly from button so can be static
    {
        if (IsPaused) return;
        IsPaused = true;
        SceneManager.LoadScene(6, LoadSceneMode.Additive);
        GetMusicController().TogglePaused();
        Time.timeScale = 0;
    }

    public void ExitGame()
    {
        Application.Quit();
    }

    private static MusicController GetMusicController() // Load music controller variable when required
    {
        if (musicController == null)
            musicController = GameObject.Find("Music Controller")?.GetComponent<MusicController>();
        return musicController;
    }
}
