﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MenuAudioSpectrum : MonoBehaviour
{
    public GameObject SpectrumBar;
    private GameObject SpectrumParent;
    private readonly GameObject[] bars = new GameObject[64];
    private AudioSource audioSource;

    void Awake()
    {
        SpectrumParent = GameObject.Find("AudioSpectrum");
        audioSource = GameObject.Find("Menu Music").GetComponent<AudioSource>();
        for (int i = 0; i < 64; ++i)
        {
            // Create line of bars between x = -8 and x = -2, with no gap between
            bars[i] = Instantiate(SpectrumBar, SpectrumParent.transform);
            float dist = (8 - 2) / 64f;
            bars[i].transform.localScale = new Vector3(dist, 1, 1);
            bars[i].transform.position = new Vector3(-8 + dist * i, 0, 0);
        }
    }

    void Update()
    {
        float[] spectrumData = new float[64];

        audioSource.GetSpectrumData(spectrumData, 0, FFTWindow.Rectangular);

        for (int i = 1; i < spectrumData.Length - 1; ++i)
        {
            Transform nextCube = bars[i].transform;
            // Reduce the difference between the largest values and smallest values
            float desiredSize = Mathf.Clamp(Mathf.Pow(spectrumData[i] * 100, 1/2f), 0.1f, 10f);
            Vector3 newScale = new Vector3(nextCube.localScale.x, desiredSize, 1);
            nextCube.localScale = Vector3.Lerp(nextCube.localScale, newScale, 3 * Time.deltaTime); // Lerp to avoid jaggedness
        }
    }
}
