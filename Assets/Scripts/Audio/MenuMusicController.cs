﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MenuMusicController : MonoBehaviour
{
    public static MenuMusicController MenuMusic;
    public GameObject go;
    private AudioSource audioSource;
    void Awake()
    {
        if (MenuMusic == null)
        {
            go = gameObject; // Store a reference to this gameObject
            DontDestroyOnLoad(gameObject); // Persist across all menu scenes
            MenuMusic = this;
        } else if (MenuMusic != this)
        {
            Destroy(gameObject); // But do not allow multiple instances of this gameObject (such as when returning to the menu scene)
        }
        
    }
    void Start()
    {
        audioSource = GetComponent<AudioSource>();
        audioSource.volume = PlayerPrefs.GetFloat("Volume", 1); // Set volume based on saved settings
    }
}
