﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MusicController : MonoBehaviour
{
    private AudioSource audioSource;

    public void LoadSong(AudioClip song)
    {
        Stop();
        audioSource.clip = song;
    }

    public void Play(float delay = 0)
    {
        audioSource.PlayDelayed(delay);
    }

    public void Stop()
    {
        audioSource.Stop();
    }

    public void Restart()
    {
        Stop();
        Play();
    }

    public void TogglePaused()
    {
        if (audioSource.isPlaying)
            audioSource.Pause();
        else
            audioSource.UnPause();
    }

    public void Seek(float time)
    {
        audioSource.time = time / 1000;
    }

    public void SetVolume(float volume)
    {
        volume = Mathf.Clamp(volume, 0, 1);
        audioSource.volume = volume;
    }
    
    // Start is called before the first frame update
    void Start()
    {
        audioSource = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {
        // For testing
        if (Input.GetKeyDown(KeyCode.R))
            Restart();
        else if (Input.GetKeyDown(KeyCode.P))
            TogglePaused();
    }
}
