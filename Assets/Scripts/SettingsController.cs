﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SettingsController : MonoBehaviour
{
    private Slider volumeSlider;
    private Text volumeText;
    private Text tooltip;

    private int difficultyMode;
    void Start()
    {
        tooltip = GameObject.Find("TooltipText").GetComponent<Text>();
        volumeSlider = GameObject.Find("VolumeSlider").GetComponent<Slider>();
        volumeText = GameObject.Find("VolumeValue").GetComponent<Text>();
        float vol = PlayerPrefs.GetFloat("Volume", 1);
        volumeSlider.value = vol;
        SetVolume();

        difficultyMode = PlayerPrefs.GetInt("DifficultyMode", 1);
        GameObject difficultyToggle = null;
        if (difficultyMode == 0)
            difficultyToggle = GameObject.Find("CasualToggle");
        else if (difficultyMode == 1)
            difficultyToggle = GameObject.Find("NormalToggle");
        else if (difficultyMode == 2)
            difficultyToggle = GameObject.Find("InsaneToggle");

        if (difficultyToggle != null)
            difficultyToggle.GetComponent<Toggle>().isOn = true; // Set the correct toggle button to on
    }

    public void SetVolume() // Update label
    {
        float val = volumeSlider.value;
        volumeText.text = val.ToString("0.##");
    }

    public void SetMode(int mode)
    {
        // 0 = casual (inf lives), 1 = normal (3 lives), 2 = insane (1 life)
        difficultyMode = mode;
    }

    public void SetAvatar(String avatar)
    {

    }

    public void SetTooltip(String text)
    {
        tooltip.text = text;
    }

    public void ApplySettings() // Apply our settings live so that can be previewed (music volume)
    {
        float vol = volumeSlider.value;

        GameObject[] audio = GameObject.FindGameObjectsWithTag("Audio");
        foreach (GameObject go in audio)
        {
            go.GetComponent<AudioSource>().volume = vol;
        }
    }

    public void RevertSettings() // Revert previewed settings
    {
        float vol = PlayerPrefs.GetFloat("Volume", 1);

        GameObject[] audio = GameObject.FindGameObjectsWithTag("Audio");
        foreach (GameObject go in audio)
        {
            go.GetComponent<AudioSource>().volume = vol;
        }
    }

    public void SaveSettings() // Save changes
    {
        float vol = volumeSlider.value;
        PlayerPrefs.SetFloat("Volume", vol);
        PlayerPrefs.SetInt("DifficultyMode", difficultyMode);
    }
}
