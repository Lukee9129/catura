﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraInit : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        // Add bounding box to each side of the camera so the player can't leave

        Camera cam = Camera.main;
        float height = 2 * cam.orthographicSize;
        float width = height * cam.aspect;

        BoxCollider2D topCollider = gameObject.AddComponent<BoxCollider2D>();
        BoxCollider2D rightCollider = gameObject.AddComponent<BoxCollider2D>();
        BoxCollider2D bottomCollider = gameObject.AddComponent<BoxCollider2D>();
        BoxCollider2D leftCollider = gameObject.AddComponent<BoxCollider2D>();

        float extra = 1; // To prevent bounding box area = 0

        topCollider.size = new Vector2(width, extra);
        topCollider.offset = new Vector2(0, (height + extra) / 2);

        rightCollider.size = new Vector2(extra, height);
        rightCollider.offset = new Vector2((width + extra) / 2, 0);

        bottomCollider.size = new Vector2(width, extra);
        bottomCollider.offset = new Vector2(0, -(height + extra) / 2);

        leftCollider.size = new Vector2(extra, height);
        leftCollider.offset = new Vector2(-(width + extra) / 2, 0);
    }
}
