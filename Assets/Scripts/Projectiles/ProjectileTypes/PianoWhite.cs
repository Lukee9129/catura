﻿using UnityEngine;

public class PianoWhite : ProjectileType
{
    public PianoWhite(GameObject projectile)
    {
        Projectile = projectile;
        OffsetAnchor = true;
        Width = 0.5f;
        Height = 2f;
    }
}
