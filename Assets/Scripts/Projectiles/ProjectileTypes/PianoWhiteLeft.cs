﻿using UnityEngine;

public class PianoWhiteLeft : ProjectileType
{
    public PianoWhiteLeft(GameObject projectile)
    {
        Projectile = projectile;
        OffsetAnchor = true;
        Width = 0.5f;
        Height = 2f;
    }
}
