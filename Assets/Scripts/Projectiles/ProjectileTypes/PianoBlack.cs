﻿using UnityEngine;

public class PianoBlack : ProjectileType
{
    public PianoBlack(GameObject projectile)
    {
        Projectile = projectile;
        OffsetAnchor = true;
        Width = 0.4f;
        Height = 2f;
    }
}
