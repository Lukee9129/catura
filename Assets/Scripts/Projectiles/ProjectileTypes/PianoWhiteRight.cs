﻿using UnityEngine;

public class PianoWhiteRight : ProjectileType
{
    public PianoWhiteRight(GameObject projectile)
    {
        Projectile = projectile;
        OffsetAnchor = true;
        Width = 0.5f;
        Height = 2f;
    }
}
