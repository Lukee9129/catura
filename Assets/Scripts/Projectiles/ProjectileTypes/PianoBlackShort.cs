﻿using UnityEngine;

public class PianoBlackShort : ProjectileType
{
    public PianoBlackShort(GameObject projectile)
    {
        Projectile = projectile;
        OffsetAnchor = true;
        Width = 0.4f;
        Height = 1.2f;
    }
}
