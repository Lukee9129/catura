﻿using UnityEngine;

public class PianoWhiteMid : ProjectileType
{
    public PianoWhiteMid(GameObject projectile)
    {
        Projectile = projectile;
        OffsetAnchor = true;
        Width = 0.5f;
        Height = 2f;
    }
}
