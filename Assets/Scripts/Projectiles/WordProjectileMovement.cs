﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class WordProjectileMovement : MonoBehaviour
{
    public float WordProjectileScale;
    private RectTransform rt;
    private WordProjectile projectile;
    private WordTelegraph telegraph;
    private Vector3 startPosition;
    private Vector3 endPosition;
    private bool isAtDestination = false;
    private bool shouldUpdate = false;
    private float distance;
    private float timeAtDestination = 0;

    public void Setup(WordProjectile projectile, WordTelegraph telegraph)
    {
        this.projectile = projectile;
        this.telegraph = telegraph;
        Text txt = GetComponent<Text>();
        txt.text = projectile.Word;
        txt.font = projectile.TextFont;
        txt.fontSize = projectile.FontSize;
        txt.color = projectile.TextColor;

        BoxCollider2D bc = GetComponent<BoxCollider2D>();
        bc.size = new Vector2(txt.preferredWidth, txt.preferredHeight);

        rt = GetComponent<RectTransform>();

        startPosition = Camera.main.ViewportToWorldPoint(new Vector3(projectile.Location.x, projectile.IsTopDown ? 1 : 0, Camera.main.nearClipPlane));
        startPosition *= WordProjectileScale;
        startPosition.y += (txt.preferredHeight / 2) * (projectile.IsTopDown ? 1 : -1); // Offset slightly so the words always start off screen!
        endPosition = Camera.main.ViewportToWorldPoint(new Vector3(projectile.Location.x, projectile.Location.y, Camera.main.nearClipPlane));
        endPosition *= WordProjectileScale;

        rt.anchoredPosition = startPosition;

        distance = Vector2.Distance(startPosition, endPosition);
    }

    public void Begin()
    {
        shouldUpdate = true;
    }
    void Update()
    {
        if (!shouldUpdate) return;

        if (!isAtDestination)
        {
            float currentDistance = Vector2.Distance(rt.anchoredPosition, endPosition);
            float interp = 1 - (currentDistance / distance);
            interp = Mathf.Round(interp * 100) / 100f; // Round it to 2dp to speed up the end of the easing
            float speed = Mathf.Lerp(projectile.Speed * Time.deltaTime, 0, interp); // Easing
            rt.anchoredPosition = Vector2.MoveTowards(rt.anchoredPosition, endPosition, speed);
            if (Mathf.Approximately(interp, 1)) // i.e. we're close enough to end destination
            {
                isAtDestination = true;
            }
        }
        else
        {
            if (timeAtDestination < projectile.Duration)
            { // Wait for projectile duration at destination
                timeAtDestination += Time.deltaTime;
                telegraph.UpdateFadeStep(timeAtDestination, projectile.Duration);
            }
            else
            { // Fall back off screen
                float currentDistance = Vector2.Distance(rt.anchoredPosition, startPosition);
                float interp = currentDistance / distance;
                interp = Mathf.Floor(interp * 100) / 100f;
                float speed = Mathf.Lerp(projectile.Speed * Time.deltaTime, 0, interp); // Easing (opposite of before)
                rt.anchoredPosition = Vector2.MoveTowards(rt.anchoredPosition, startPosition, speed);

                if (Mathf.Approximately(interp, 0)) // Reached start position again
                {
                    // Destroy parent of this gameObject (the parent contains both the telegraph and the projectile)
                    Destroy(gameObject.transform.parent.gameObject, 10); // Wait 10 seconds, so any animations can finish
                }
            }
        }
      
    }
}
