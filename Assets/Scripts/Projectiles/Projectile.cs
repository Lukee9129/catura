﻿using UnityEngine;

public class Projectile
{
    public ProjectileType ProjType { get; }
    public Vector2 StartLocation { get; set; }
    public Vector2 EndLocation { get; set; }
    public float MoveSpeed { get; set; }
    public float InitialRotation { get; set; }
    public float RotationSpeed { get; set; }
    public int Order { get; set; }

    public Projectile(ProjectileType projType, Vector2 startLocation, Vector2 endLocation, float moveSpeed, float initialRotation = 0, float rotationSpeed = 0, int order = 0)
    {
        ProjType = projType;
        StartLocation = startLocation;
        EndLocation = endLocation;
        MoveSpeed = moveSpeed;
        InitialRotation = initialRotation;
        RotationSpeed = rotationSpeed;
        Order = order;
    }
}
