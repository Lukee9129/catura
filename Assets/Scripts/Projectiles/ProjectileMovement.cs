﻿using UnityEngine;

public class ProjectileMovement : MonoBehaviour
{
    private Projectile projectile;

    private Vector2 dir;
    private Rigidbody2D rb;
    
    public void Begin(Projectile projectile)
    {
        this.projectile = projectile;
        dir = Vector3.Normalize(projectile.EndLocation- projectile.StartLocation);
        rb = GetComponent<Rigidbody2D>();
        transform.position = new Vector3(projectile.StartLocation.x, projectile.StartLocation.y, -projectile.Order);
        rb.drag = 0; // Disable drag as we want constant movespeed

        // Set velocity based on movespeed and direction
        rb.velocity = dir * projectile.MoveSpeed;
        rb.angularVelocity = -projectile.RotationSpeed;
    }

    // FixedUpdate used for physics updates
    void FixedUpdate()
    {
        if (projectile.ProjType.DestroyAtDestination)
        {
            Vector2 pos = transform.position;
            // If we've reached the end location we can destroy this gameobject
            float maxDistance = Vector2.Distance(projectile.EndLocation, projectile.StartLocation);
            float distance = Vector2.Distance(pos, projectile.StartLocation);
            if (distance > maxDistance)
            {
                DebugModule.RemoveActiveProjectile(gameObject);
                Destroy(gameObject);
            }
        }
    }
}
