﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class WordTelegraph : MonoBehaviour
{
    private WordProjectile projectile;
    private Text txt;
    private LineRenderer lr;
    private Vector3 lineCentre;
    private float lineLength;
    public void Begin(WordProjectile projectile, WordProjectileMovement movement)
    {
        this.projectile = projectile;
        RectTransform rt = GetComponent<RectTransform>();
        txt = GetComponent<Text>();
        txt.text = projectile.Word;
        txt.font = projectile.TextFont;
        txt.fontSize = projectile.FontSize;
        
        Vector3 position = Camera.main.ViewportToWorldPoint(new Vector3(projectile.Location.x, projectile.Location.y, Camera.main.nearClipPlane));
        rt.anchoredPosition = position * movement.WordProjectileScale;

        lr = GetComponent<LineRenderer>();
        Vector3 lineStart = position;
        lineStart.x -= txt.preferredWidth / 2 / movement.WordProjectileScale;
        lineStart.y += txt.preferredHeight / 2 / movement.WordProjectileScale * (projectile.IsTopDown ? 1 : -1);
        lineStart.z = 1;
        Vector3 lineEnd = new Vector3(lineStart.x + txt.preferredWidth / movement.WordProjectileScale, lineStart.y, 1);
        lr.SetPositions(new Vector3[] { lineStart, lineEnd });

        lineCentre = (lineStart + lineEnd) / 2;
        lineLength = lineEnd.x - lineStart.x;

        StartCoroutine(TelegraphAndExecute(movement));
    }

    public void UpdateFadeStep(float timePassed, float duration)
    {
        if (timePassed <= duration)
        {
            // Reduce the line renderer, anchored at centre, over the duration
            float progress = timePassed / duration;
            float currentLength = lineLength * (1 - progress);
            float halfLength = currentLength / 2;
            Vector3 lineStart = lineCentre;
            lineStart.x -= halfLength;
            Vector3 lineEnd = lineCentre;
            lineEnd.x += halfLength;
            // Set line ends
            lr.SetPositions(new Vector3[] { lineStart, lineEnd });
        }
        else
        {
            // Fade out and stop telegraph
            lr.enabled = false; // Hide any remaining line
            StartCoroutine(TelegraphFadeOut());
        }
    }

    IEnumerator TelegraphAndExecute(WordProjectileMovement movement)
    {
        Color startColor = new Color(projectile.TextColor.r, projectile.TextColor.g, projectile.TextColor.b, 0); // Start invis so we can fade in
        txt.color = startColor;
        Color targetColor = new Color(projectile.TextColor.r, projectile.TextColor.g, projectile.TextColor.b, 0.25f); // End at 25% opacity
        for (float f = 0; f <= 1; f += 0.01f)
        {
            txt.color = Color.Lerp(startColor, targetColor, f);
            yield return new WaitForSeconds(0.01f);
        }

        movement.Begin(); // Telegraph period ended, move the actual projectile on screen
    }


    IEnumerator TelegraphFadeOut()
    {
        // Fade out remaining telegraph and destroy
        Color startColor = new Color(projectile.TextColor.r, projectile.TextColor.g, projectile.TextColor.b, 0.25f);
        Color targetColor = new Color(projectile.TextColor.r, projectile.TextColor.g, projectile.TextColor.b, 0);
        for (float f = 0; f <= 1; f += 0.01f)
        {
            txt.color = Color.Lerp(startColor, targetColor, f);
            yield return new WaitForSeconds(0.01f);
        }
        Destroy(gameObject);
    }
}
