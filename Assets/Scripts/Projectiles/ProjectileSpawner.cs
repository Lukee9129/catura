﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProjectileSpawner : MonoBehaviour
{
    public GameObject WordProjectileTemplate;
    private GameObject wordProjectileCanvas;
    private GameObject parent;
    private int projCount = 0;
    // Start is called before the first frame update
    void Start()
    {
        parent = GameObject.Find("Projectiles"); // Parent of all projectiles
        wordProjectileCanvas = GameObject.Find("Word Projectile Canvas");
    }
    
    public void SpawnProjectiles(Projectile[] projectiles, float time = 0f)
    {
        foreach (Projectile proj in projectiles)
        {
            GameObject nextProjectile = Instantiate(proj.ProjType.Projectile, new Vector3(proj.StartLocation.x, proj.StartLocation.y, -proj.Order), Quaternion.identity);
            nextProjectile.name = $"Projectile{++projCount}";
            nextProjectile.transform.parent = parent.transform;

            DebugModule.AddActiveProjectile(time, nextProjectile, proj);

            nextProjectile.transform.Rotate(Vector3.forward, -proj.InitialRotation);
            ProjectileMovement projectileMovement = nextProjectile.GetComponent<ProjectileMovement>();
            projectileMovement.Begin(proj);
        }
    }

    public void SpawnWords(WordProjectile[] words, float time = 0f)
    {
        foreach (WordProjectile proj in words)
        {
            GameObject nextWordProjectile = Instantiate(WordProjectileTemplate, wordProjectileCanvas.transform);
            nextWordProjectile.name = $"WordProjectile{proj.Word}";

            WordProjectileMovement movement = nextWordProjectile.GetComponentInChildren<WordProjectileMovement>();
            WordTelegraph telegraph = nextWordProjectile.GetComponentInChildren<WordTelegraph>();
            movement.Setup(proj, telegraph);
            telegraph.Begin(proj, movement);
        }
    }

    public void RunEvents(Event[] events, float time = 0f)
    {
        foreach (Event eve in events)
        {
            StartCoroutine(eve.RunEvent());
        }
    }
}
