﻿using System;
using UnityEngine;

public abstract class ProjectileType
{
    public GameObject Projectile;
    public bool DestroyAtDestination = true;
    public bool OffsetAnchor;
    public float Width;
    public float Height;

    // If the projectile changes over time, use this function to apply changes.
    public virtual bool ActionOverTime()
    {
        return true;
    }
}