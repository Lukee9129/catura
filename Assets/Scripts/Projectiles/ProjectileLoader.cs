﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProjectileLoader : MonoBehaviour
{
    public static readonly Dictionary<string, ProjectileType> ProjectileTypes = new Dictionary<string, ProjectileType>();
    public bool IsLoaded = false;
    public GameObject[] Projectiles;

    private static ProjectileLoader projectileLoader;

    void Awake()
    {
        if (projectileLoader == null)
        {
            // Only load projectiles once!
            DontDestroyOnLoad(gameObject);
            projectileLoader = this;
            SetupProjectiles();
        } else if (projectileLoader != this) {
            Destroy(gameObject);
        }
    }

    void SetupProjectiles()
    {
        ProjectileTypes.Add("piano_white", new PianoWhite(Projectiles[0]));
        ProjectileTypes.Add("piano_black", new PianoBlack(Projectiles[1]));
        ProjectileTypes.Add("piano_white_left", new PianoWhiteLeft(Projectiles[2]));
        ProjectileTypes.Add("piano_white_mid", new PianoWhiteMid(Projectiles[3]));
        ProjectileTypes.Add("piano_white_right", new PianoWhiteRight(Projectiles[4]));
        ProjectileTypes.Add("piano_black_short", new PianoBlackShort(Projectiles[5]));
        IsLoaded = true;
    }
}
