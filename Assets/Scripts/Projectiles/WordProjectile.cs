﻿using UnityEngine;

public class WordProjectile
{
    public string Word { get; set; }
    public int FontSize { get; set; }
    public Font TextFont { get; set; }
    public Color TextColor { get; set; }
    public Vector2 Location { get; set; }
    public bool IsTopDown { get; set; }
    public float Duration { get; set; }
    public float Speed { get; set; }

    public WordProjectile(string word, int fontSize, Font textFont, Color textColor, Vector2 location, bool isTopDown, float duration, float speed)
    {
        Word = word;
        FontSize = fontSize;
        TextFont = textFont;
        TextColor = textColor;
        Location = location;
        IsTopDown = isTopDown;
        Duration = duration;
        Speed = speed;
    }
}
