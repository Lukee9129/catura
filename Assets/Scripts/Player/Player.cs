﻿using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Player : MonoBehaviour
{
    public float MoveSpeed = 10.0f;
    public float DamageGraceTime = 1.0f; // Invincibility frames last 1s
    public GameObject DeathExplosion;
    public bool isLocked = false;

    private SpriteRenderer sprite;
    private Rigidbody2D rb;
    private float timeSinceDamaged = float.MaxValue;
    private int lives = 1;
    private bool isImmortal = false;
    // Start is called before the first frame update
    void Start()
    {
        sprite = GetComponent<SpriteRenderer>();
        rb = GetComponent<Rigidbody2D>();

        int difficultyMode = PlayerPrefs.GetInt("DifficultyMode", 1);
        if (difficultyMode == 0)
            isImmortal = true;
        else if (difficultyMode == 2)
            lives = 1;
        else
            lives = 3;
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetButtonDown("Cancel"))
        {
            SceneController.PauseGame();
        }

        if (timeSinceDamaged < DamageGraceTime) timeSinceDamaged += Time.deltaTime;
    }

    // FixedUpdate used for physics updates
    void FixedUpdate()
    {
        if (isLocked) return; // If player movement is locked, ignore input
        float vertical = Input.GetAxisRaw("Vertical");
        float horizontal = Input.GetAxisRaw("Horizontal");
        Vector2 movement = new Vector2(horizontal, vertical).normalized * MoveSpeed;
        rb.velocity = movement;
    }

    void OnTriggerEnter2D(Collider2D col)
    {
        DebugModule.PrintPlayerCollision(col);
        if (isImmortal) return;
        // Here we will deal with losing lives / losing the game
        if (col.CompareTag("Projectile"))
        {
            TryTakeDamage();
        }
    }

    private void TryTakeDamage()
    {
        if (timeSinceDamaged >= DamageGraceTime) // Grace period (invincibility frames) since the player last took damage
        {
            timeSinceDamaged = 0;
            lives--;
            if (lives == 0) Dead();
            else StartCoroutine(AnimateDamageTaken());
        }
    }

    private void Dead()
    {
        Instantiate(DeathExplosion, transform.position, Quaternion.identity);
        gameObject.GetComponent<Renderer>().enabled = false; // Hide us
        StartCoroutine(LaunchGameOverScreen());
    }

    private IEnumerator AnimateDamageTaken()
    {
        Color startColor = Color.white;
        Color endColor = new Color(0.25f, 0.25f, 0.25f);

        // Fade out and in 2 times over DamageGraceTime
        for (int i = 0; i < 2; ++i)
        {
            for (float f = 0; f <= 1; f += 0.02f)
            {
                sprite.color = Color.Lerp(startColor, endColor, f);
                yield return new WaitForSeconds(0.01f);
            }

            for (float f = 0; f <= 1; f += 0.02f)
            {
                sprite.color = Color.Lerp(endColor, startColor, f);
                yield return new WaitForSeconds(0.01f);
            }
        }
    }

    private IEnumerator LaunchGameOverScreen()
    {
        yield return new WaitForSeconds(3f); // Wait 3 seconds then go
        SceneManager.LoadScene(5);
    }
}
