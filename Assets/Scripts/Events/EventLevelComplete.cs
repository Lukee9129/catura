﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class EventLevelComplete : Event
{
    public override IEnumerator RunEvent()
    {
        yield return new WaitForSeconds(3f); // Wait 3 seconds then go
        SceneManager.LoadScene(4); // Game win screen
    }
}
