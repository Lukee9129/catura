﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EventLoader : MonoBehaviour
{
    public static readonly Dictionary<string, Event> Events = new Dictionary<string, Event>();
    public bool IsLoaded = false;

    private static EventLoader eventLoader;
    void Awake()
    {
        if (eventLoader == null)
        {
            // Only load events once!
            DontDestroyOnLoad(gameObject);
            eventLoader = this;
            SetupEvents();
        }
        else if (eventLoader != this)
        {
            Destroy(gameObject);
        }
    }

    void SetupEvents()
    {
        Events.Add("level_complete", new EventLevelComplete());
        IsLoaded = true;
    }
}
