﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Color = UnityEngine.Color;

public class DebugModule : MonoBehaviour
{
    private const bool IsDebugActive = false;
    private static List<ActiveProjectile> activeProjectiles = new List<ActiveProjectile>();

    private GUIStyle textStyle = new GUIStyle();
    private Vector2 size = new Vector2(100, 100);

    void Start()
    {
        if (!IsDebugActive) enabled = false;
        textStyle.normal.textColor = Color.red;
        textStyle.fontSize = 24;
    }

    void OnGUI()
    {
        foreach (ActiveProjectile activeProjectile in activeProjectiles)
        {
            Vector2 pos = Camera.main.WorldToScreenPoint(activeProjectile.GO.transform.position);
            pos.y = Screen.height - pos.y;
            string label = $"Time: {activeProjectile.TimeSpawned}";
            GUI.Label(new Rect(pos, size), label, textStyle);
        }
    }

    public static void PrintPlayerCollision(Collider2D col)
    {
        if (!IsDebugActive) return;
        Debug.Log("Player has collided with " + col.gameObject.name);
    }

    public static void AddActiveProjectile(float time, GameObject gameObject, Projectile data)
    {
        if (!IsDebugActive) return;
        activeProjectiles.Add(new ActiveProjectile(time, gameObject, data));
    }

    public static void RemoveActiveProjectile(GameObject gameObject)
    {
        if (!IsDebugActive) return;
        activeProjectiles.RemoveAll(x => x == null || x.GO == null || x.GO == gameObject);
    }

    public static void ClearActiveProjectiles()
    {
        if (!IsDebugActive) return;
        activeProjectiles.Clear();
    }

    private class ActiveProjectile
    {
        public readonly float TimeSpawned;
        public readonly GameObject GO;
        public readonly Projectile Data;

        public ActiveProjectile(float timeSpawned, GameObject gameObject, Projectile data)
        {
            this.TimeSpawned = timeSpawned;
            this.GO = gameObject;
            this.Data = data;
        }
    }
}
