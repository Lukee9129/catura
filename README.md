# Catura
Catura is a music-based bullet hell game created in the Unity engine.
Please note that the music files have been removed from this repository to avoid copyright issues.

## Getting Started
To build the project yourself you will need to download Unity (see Prerequisites) then follow the steps in 'Building' below.

### Prerequisites
* [Unity 2019.2.8(f1)](https://unity3d.com/get-unity/download/archive)

### Minimum system requirements
As taken from the [Unity website](https://unity3d.com/unity/system-requirements):

#### For building
OS: Windows 7 SP1+, 8, 10, 64-bit versions only.
CPU: SSE2 instruction set support.
GPU: Graphics card with DX10 (shader model 4.0) capabilities.

#### For running
OS: Windows 7 SP1+
Graphics card with DX10 (shader model 4.0) capabilities.
CPU: SSE2 instruction set support.
Audio: An audio device (speakers, headphones, etc.) and system that can play audio.

### Building
1. Clone the repository.
2. Launch Unity.
3. Open the project in Unity. If using Unity Hub you can go to ```Projects > Add > Navigate to the Catura folder > Click 'Catura' in the list of Projects``` - ensure Unity Version is set to 2019.2.8f1 (See Unity Hub image below). If using Unity (without Unity Hub) you can ```click Open > Navigate to the Catura folder > Click 'Catura' in the list of Projects (Projects tab)``` (See Unity image below).
4. Go to File > Build Settings > Build > Choose a folder to build in.

Unity Hub:
![Unity Hub Instructions](https://i.imgur.com/5F8tXOq.jpg)

Unity:
![Unity Instructions](https://i.imgur.com/3KkPTEa.jpg)

### Running
If downloading release (.zip), extract all files to a folder before running!
Navigate to extraction folder / build folder > Run Catura.exe > Play the game.

### Level-data file format
Levels in Catura load all their information from level-data files. These files follow a format:
```
[meta]
Song Title
Artist Name
Difficulty
[/meta]
[proj]
# Comment
Projectile
Lyric Projectile
Event
[/proj]
```
Where 'Difficulty' can be 'Easy', 'Medium', 'Hard' or 'Impossible'.
Projectiles, lyric projectiles and events are defined in the [proj][/proj] section.

Projectiles are defined as follows:
```
<1>,<2>,<3>,<4>,<5>,<6>,<7>,<8>,<9>,<10>

<1>	Time since alpha [Integer] – The time, in milliseconds, since the start of the level, i.e. when the projectile should be spawned.
<2>	Projectile type [String] – The identifier for the predefined projectile type to use.
<3>	Start viewport x [Decimal] – The x viewport* coordinate of the start position of the projectile.
<4>	Start viewport y [Decimal] – The y viewport coordinate of the start position of the projectile. 
<5>	End viewport x [Decimal] – The x viewport coordinate of the end position of the projectile.
<6>	End viewport y [Decimal] – The y viewport coordinate of the end position of the projectile.
<7>	Linear move speed [Decimal] – How quickly the projectile moves towards the end position (in units/s)
<8>	Initial rotation [Decimal] – The clockwise rotation of the projectile on spawn (in degrees). Defaults to 0.
<9>	Rotational move speed [Decimal] – How quickly to rotate clockwise (in radians/s). Defaults to 0.
<10>	Order [Integer] – The order to render this projectile in relative to other projectiles. Higher values appear on top. Defaults to 0.
```

Word projectiles are defined as follows:
```
<1>,<2>,<3>,<4>,<5>,<6>,<7>,<8>,<9>,<10>,<11>,<12>

<1>	Time since alpha [Integer] – The time, in milliseconds, since the start of the level, i.e. when the projectile should be spawned.
<2>	Word [String] – The text that the projectile will be displaying, prefixed with ‘word_’ for readability, for example ‘word_Example’ would display ‘Example’. Can contain punctuation.
<3>	Font size [Integer] – The font size to use (in pt).
<4>	Font name [String] – The filename of the font to use, with extension omitted. Fonts must be placed in the ‘Resources/Fonts’ folder. TrueType Fonts (.ttf) and OpenType Fonts (.otf) are supported [43].  If the file does not exist it defaults to Arial.
<5>	Font colour R [Decimal] – The red component of the font colour to use. From 0 (0) to 1 (255).
<6>	Font colour G [Decimal] – The green component of the font colour to use. From 0 (0) to 1 (255).
<7>	Font colour B [Decimal] – The blue component of the font colour to use. From 0 (0) to 1 (255).
<8>	Viewport location x [Decimal] – The x viewport coordinate of the destination for the word projectile.
<9>	Viewport location y [Decimal] - The y viewport coordinate of the destination for the word projectile.
<10>	Is from top? [Boolean] – If the word projectile should come from the top of the screen. If false it comes from the bottom instead. Can be 0 (false) or 1 (true).
<11>	Duration [Decimal] – How long the word projectile should stay at its destination for (in seconds).
<12>	Speed to reach destination [Decimal] – How quickly the word projectile should move towards its destination (in units/s).
```

Events are defined as follows:
```
<1>,<2>

<1>	Time since alpha [Integer] – The time, in milliseconds, since the start of the level, i.e. when the event should be executed.
<2>	Event name [String] – The identifier for the predefined event to use, prefixed with ‘event_’ for readability, for example ‘event_Example’ would execute the event with identifier ‘Example’.
```
Each value is comma-separated with no whitespace in between (see example below). Viewport location refers to position on screen ranging from 0 to 1 with  (0,0) being bottom-left and (1,1) being top right. 'Projectile initial rotation' is an angle in degrees.

Lines in the [proj] section prefixed with a hash '#' character are ignored.

An example of a song with a projectile, lyric projectile, event and comment:
```
[meta]
It's Hard to Say Goodbye
Michael Ortega
Easy
[/meta]
[proj]
# Example!
432,piano_white,1,0.75,0,0.75,4,90,0,0
800,word_Hello,32,Arial,1,1,1,0.1,0.85,0,0.2,2500
1000,event_level_complete
[/proj]
```

### Versions
1.0 - Minimum viable product.

2.0 - Final submission.

## Authors
* Luke Dixon - Game design and programming.
* Professor Richard Bartle - Project Manager.

## Music acknowledgements
* Ortega, M. (2010). It's Hard to Say Goodbye. [online] Youtube. Available at: https://www.youtube.com/watch?v=a_Am4cHMBKM [Accessed 15 Nov. 2019].
* Hill, M. (2016). Down. [online] Youtube. Available at: https://www.youtube.com/watch?v=DpMfP6qUSBo [Accessed 25 Jan. 2020].
